//
//  FloridAppDelegate.m
//  Florid
//
//  Created by Eric Fredricksen on 8/24/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

#import "FloridAppDelegate.h"
#import "EAGLView.h"

@implementation FloridAppDelegate

@synthesize window;
@synthesize glView;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	application.applicationSupportsShakeToEdit = YES;
    [glView startAnimation];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [glView stopAnimation];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    [glView startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [glView stopAnimation];
}

- (void)dealloc
{
    [window release];
    [glView release];

    [super dealloc];
}

@end
