//
//  Shader.fsh
//  fluid
//
//  Created by Eric Fredricksen on 8/14/10.
//  Copyright __MyCompanyName__ 2010. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
