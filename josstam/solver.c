// Based on:
// Jos Stam, "Real-Time Fluid Dynamics for Games". Proceedings of the Game 
// Developer Conference, March 2003. 
// http://www.dgp.toronto.edu/people/stam/reality/Research/pub.html
// http://www.dgp.toronto.edu/people/stam/reality/Research/pdf/GDC03.pdf
// http://www.dgp.toronto.edu/people/stam/reality/Research/zip/CDROM_GDC03.zip

#define IX(i,j) ((i)+(N+2)*(j))
#define FOR_EACH_CELL for (int i=1 ; i<=N ; i++ ) for (int j=1 ; j<=N ; j++ )


// Contribute density at the specified rate
void add_source(int N, float* x, float* dxdt, float dt)
{
	const int size = (N+2)*(N+2);
	for (int i = 0; i < size; i++) 
		x[i] += dt * dxdt[i];
}


// Set boundary conditions, assuming we're in a box. The opaque "b" parameter 
// may be 1 to indicate a horizontal component, 2 to indicate a vertical 
// component, and 0 otherwise.
void set_boundaries(int N, int b, float* x)
{
	for (int i = 1; i <= N; i++) {
		x[IX(0  ,i  )] = b==1 ? -x[IX(1,i)] : x[IX(1,i)];
		x[IX(N+1,i  )] = b==1 ? -x[IX(N,i)] : x[IX(N,i)];
		x[IX(i,  0  )] = b==2 ? -x[IX(i,1)] : x[IX(i,1)];
		x[IX(i,  N+1)] = b==2 ? -x[IX(i,N)] : x[IX(i,N)];
	}
	x[IX(0  ,0  )] = 0.5f * (x[IX(1,0  )] + x[IX(0  ,1)]);
	x[IX(0  ,N+1)] = 0.5f * (x[IX(1,N+1)] + x[IX(0  ,N)]);
	x[IX(N+1,0  )] = 0.5f * (x[IX(N,0  )] + x[IX(N+1,1)]);
	x[IX(N+1,N+1)] = 0.5f * (x[IX(N,N+1)] + x[IX(N+1,N)]);
}


// Approximate a solution to system of linear equations 
//   X' = (X + a * neighbors(X)) / c
// using Gauss-Seidel relaxation
void linear_solve(int N, int b, float * x, float * x0, float a, float c)
{
	for (int k = 0; k < 20; k++) {
		FOR_EACH_CELL {
			x[IX(i,j)] = (x0[IX(i,j)] + a * (x[IX(i-1,j)] + x[IX(i+1,j)] + 
											 x[IX(i,j-1)] + x[IX(i,j+1)])) / c;
		}
		set_boundaries(N, b, x);
	}
}


// Diffuse via Gauss-Seidel relaxation
void diffuse(int N, int b, float * x, float * x0, float diff, float dt)
{
	float a = dt * diff * N * N;
	if (a == 0.0f)
		for (int i = 0; i < (N+2) * (N+2); ++i) x[i] = x0[i];
	else
		linear_solve(N, b, x, x0, a, 1 + 4 * a);
}


// Advect via linear backtrace: Trace velocity backward from each cell center, and compute density as
// a linear combination of surrounding cells' density.
void advect(int N, int b, float * d, float * d0, float * u, float * v, float dt)
{
	const float dt0 = dt * N;
	FOR_EACH_CELL {
		float x = i - dt0 * u[IX(i,j)]; 
		if (x < 0.5f)     x = 0.5f; 
		if (x > N + 0.5f) x = N+0.5f; 
		int i0 = (int)x; 
		int i1 = i0 + 1;
		float s1 = x - i0; 
		float s0 = 1 - s1; 
		
		float y = j - dt0 * v[IX(i,j)];
		if (y < 0.5f)   y = 0.5f; 
		if (y > N+0.5f) y = N+0.5f; 
		int j0 = (int) y; 
		int j1 = j0+1;
		float t1 = y - j0; 
		float t0 = 1 - t1;

		d[IX(i,j)] = s0 * (t0 * d0[IX(i0,j0)] + t1 * d0[IX(i0,j1)]) +
					 s1 * (t0 * d0[IX(i1,j0)] + t1 * d0[IX(i1,j1)]);
	}
	set_boundaries(N, b, d);
}

// Subtract the gradient of the height field to make the preceding operation into a
// mass-conserving one
void project(int N, float* u, float* v, float* p, float* div)
{
	FOR_EACH_CELL {
		div[IX(i,j)] = -0.5f * (u[IX(i+1,j  )] - u[IX(i-1,j  )] + 
								v[IX(i,  j+1)] - v[IX(i,  j-1)]) / N;
		p[IX(i,j)] = 0;
	}
	set_boundaries(N, 0, div); 
	set_boundaries(N, 0, p);

	linear_solve(N, 0, p, div, 1, 4);

	FOR_EACH_CELL {
		u[IX(i,j)] -= 0.5f * N * (p[IX(i+1,j  )] - p[IX(i-1,j  )]);
		v[IX(i,j)] -= 0.5f * N * (p[IX(i,  j+1)] - p[IX(i,  j-1)]);
	}
	set_boundaries(N, 1, u); 
	set_boundaries(N, 2, v);
}

void density_step(int N, float* x, float* x0, float* u, float* v, float diff, float dt)
{
	add_source(N,     x, x0,       dt);
	//x[IX(0,     N/3)] = 100.0f * dt;
	//x[IX(2*N/3, N+1)] = 0;
	diffuse(   N, 0, x0,  x, diff, dt);
	advect(    N, 0, x,  x0, u, v, dt);
}

void velocity_step(int N, float* u, float* v, float* u0, float* v0, float visc, float dt)
{
	add_source(N,    u,  u0,         dt); 
	add_source(N,    v,  v0,         dt);
	//u[IX(0,     N/3)] = 10.0f;
	//v[IX(2*N/3, N+1)] = 100.0f;
	diffuse(   N, 1, u0, u,  visc,   dt);
	diffuse(   N, 2, v0, v,  visc,   dt);
	project(   N,    u0, v0, u,  v);
	advect(    N, 1, u,  u0, u0, v0, dt); 
	advect(    N, 2, v,  v0, u0, v0, dt);
	project(   N,    u,  v,  u0, v0);
}
