/*
  ======================================================================
   demo.c --- protoype to show off the simple solver
  ----------------------------------------------------------------------
   Author : Jos Stam (jstam@aw.sgi.com)
   Creation Date : Jan 9 2003

   Description:

	This code is a simple prototype that demonstrates how to use the
	code provided in my GDC2003 paper entitles "Real-Time Fluid Dynamics
	for Games". This code uses OpenGL and GLUT for graphics and interface

  =======================================================================
*/

#include <stdlib.h>
#include <stdio.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

/* macros */

#define IX(i,j) ((i)+(N+2)*(j))

/* external definitions (from solver.c) */

extern void density_step(int N, float* x, float* x0, float* u, float* v, float diff, float dt);
extern void velocity_step(int N, float* u, float* v, float* u0, float* v0, float visc, float dt);

/* global variables */

static int N = 64;             // Dimension of the computational grid
static float diffusion = 0.000f; 
static float viscosity = 0.000f; 
static float force = 300.0f;    // Force of dragging through field
static float source = 600.0f;  // Density of source (ddensity/dt)

static int dvel = 1;           // Draw velocity vectors?

static float *u, *v, *u_prev, *v_prev;  // <u,v> components of the velocity field, and previous values
static float *dens, *dens_prev;         // density field, and previous values

static int mouse_down[3] = {0,0,0};
static float omx, omy, mx, my;


/*
  ----------------------------------------------------------------------
   free/clear/allocate simulation data
  ----------------------------------------------------------------------
*/


static void free_data ( void )
{
	if ( u ) free ( u );
	if ( v ) free ( v );
	if ( u_prev ) free ( u_prev );
	if ( v_prev ) free ( v_prev );
	if ( dens ) free ( dens );
	if ( dens_prev ) free ( dens_prev );
}

void clear_data()
{
	int i, size=(N+2)*(N+2);

	for (i=0 ; i<size ; i++) {
		u[i] = v[i] = u_prev[i] = v_prev[i] = dens[i] = dens_prev[i] = 0.0f;
	}
}

void allocate_data()
{
	int size = (N+2)*(N+2);

	u			= (float*) malloc(size * sizeof(float));
	v			= (float*) malloc(size * sizeof(float));
	u_prev		= (float*) malloc(size * sizeof(float));
	v_prev		= (float*) malloc(size * sizeof(float));
	dens		= (float*) malloc(size * sizeof(float));	
	dens_prev	= (float*) malloc(size * sizeof(float));

	if (!u || !v || !u_prev || !v_prev || !dens || !dens_prev) {
		fprintf ( stderr, "cannot allocate data\n" );
		exit(1);
	}
}


/*
  ----------------------------------------------------------------------
   OpenGL specific drawing routines
  ----------------------------------------------------------------------
*/


static void draw_velocity()
{
	float h = 1.0f / N;

	glColor4f(1.0f, 0.5f, 0.5f, 1.0f);
	glLineWidth(1.0f);

	typedef struct Point2d { GLfloat x, y; } Point2d;
	const int VERTICES = N * N * 2;
	Point2d vertices[VERTICES];
	Point2d *vertex = vertices;

	for (int i = 1; i <= N; i++) {
		float x = (i - 0.5f) * h;
		for (int j = 1; j <= N; j++) {
			float y = (j - 0.5f) * h;
			vertex->x = x;
			vertex->y = y;
			++vertex;
			vertex->x = x + u[IX(i,j)] / 6.0f;
			vertex->y = y + v[IX(i,j)] / 6.0f;
			++vertex;
		}
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, 0, vertices);

	glDisableClientState(GL_COLOR_ARRAY);

	glDrawArrays(GL_LINES, 0, VERTICES);
}

typedef struct Point2dC { 
	GLfloat r,g,b,a, x,y;
} Point2dC;

void set(Point2dC* p, GLfloat c, GLfloat x, GLfloat y) {
	p->r = p->g = p->b = c; p->a = 1.0f; p->x = x; p->y = y;
}

static void draw_density()
{
	const float h = 1.0f / N;
	const int VERTICES = (N+1) * (N+1) * 6;
	
	Point2dC vertices[VERTICES];
	Point2dC *vertex = vertices;
	
	for (int i = 0; i <= N; i++) {
		float x = (i - 0.5f) * h;
		for (int j = 0; j <= N; j++) {
			float y = (j - 0.5f) * h;

			float d00 = dens[IX(i,  j)];
			float d01 = dens[IX(i,  j+1)];
			float d10 = dens[IX(i+1,j)];
			float d11 = dens[IX(i+1,j+1)];

			set(vertex++, d00, x,   y);
			set(vertex++, d10, x+h, y);
			set(vertex++, d11, x+h, y+h);
			
			set(vertex++, d11, x+h, y+h);
			set(vertex++, d01, x,   y+h);
			set(vertex++, d00, x,   y);
		}
	}

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(2, GL_FLOAT, sizeof(vertices[0]), &vertices->x);
	
	glEnableClientState(GL_COLOR_ARRAY);
	glColorPointer(4, GL_FLOAT, sizeof(vertices[0]), &vertices->r);
	
	glDrawArrays(GL_TRIANGLES, 0, VERTICES);

}

/*
  ----------------------------------------------------------------------
   relates mouse movements to forces sources
  ----------------------------------------------------------------------
*/

static void process_input(float* d, float* u, float* v, float dt)
{
	const int size = (N+2)*(N+2);

	for (int i = 0; i < size; i++) {
		u[i] = v[i] = d[i] = 0.0f;
	}

	if (mouse_down[0] || mouse_down[2]) {
		int i = (int)(mx * (N+1));
		int j = (int)(my * (N+1));

		if (i < 1 || i > N || j < 1 || j > N) 
			return;

		if ( mouse_down[0] ) {
			u[IX(i,j)] = force * (mx-omx) / dt;
			v[IX(i,j)] = force * (my-omy) / dt;
		}

		//if ( mouse_down[2] )
		{
			d[IX(i,j)] = source;
		}

		omx = mx;
		omy = my;
	}
}

/*
  ----------------------------------------------------------------------
   GLUT callback routines
  ----------------------------------------------------------------------
*/

void key_func(unsigned char key)
{
	switch ( key )
	{
		case 'c':
		case 'C':
			clear_data();
			break;

		case 'v':
		case 'V':
			dvel = !dvel;
			break;
	}
}

void mouse_func(int button, int state, float x, float y)
{
	omx = mx = x;
	omy = my = y;

	mouse_down[button] = state;
}

void motion_func(float x, float y)
{
	mx = x;
	my = y;
}

void step_simulation(float dt)
{
	process_input(dens_prev, u_prev, v_prev, dt);
	velocity_step(N, u, v, u_prev, v_prev, viscosity, dt);
	density_step( N, dens, dens_prev, u, v, diffusion, dt);
}

void display_func()
{
	draw_density();
	if(dvel)
	  draw_velocity();
}


